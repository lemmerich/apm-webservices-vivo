/**
 * IMetricsDataService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.caapm.wsdl;

public interface IMetricsDataService extends java.rmi.Remote {
    public br.com.caapm.wsdl.TimeslicedMetricDataPerAgentMetric[] getBulkMetricData(br.com.caapm.wsdl.AgentMetricName[] allMetrics, java.util.Date startTime, java.util.Date endTime, int dataFrequency) throws java.rmi.RemoteException, br.com.caapm.wsdl.IntroscopeWebServicesException;
    public br.com.caapm.wsdl.TimeslicedResultSetMetricData[] getLiveMetricData(java.lang.String agentRegex, java.lang.String metricPrefix) throws java.rmi.RemoteException, br.com.caapm.wsdl.IntroscopeWebServicesException;
    public br.com.caapm.wsdl.TimesliceGroupedMetricData[] getTopNMetricData(java.lang.String agentRegex, java.lang.String metricRegex, java.util.Date startTime, java.util.Date endTime, int dataFrequency, int topNCount, boolean decreasingOrder) throws java.rmi.RemoteException, br.com.caapm.wsdl.IntroscopeWebServicesException;
    public br.com.caapm.wsdl.TimesliceGroupedMetricData[] getMetricData(java.lang.String agentRegex, java.lang.String metricRegex, java.util.Date startTime, java.util.Date endTime, int dataFrequency) throws java.rmi.RemoteException, br.com.caapm.wsdl.IntroscopeWebServicesException;
}
