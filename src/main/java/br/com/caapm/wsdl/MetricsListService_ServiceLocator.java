/**
 * MetricsListService_ServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.caapm.wsdl;

public class MetricsListService_ServiceLocator extends org.apache.axis.client.Service implements br.com.caapm.wsdl.MetricsListService_Service {

    public MetricsListService_ServiceLocator() {
    }


    public MetricsListService_ServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MetricsListService_ServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for MetricsListService
    private java.lang.String MetricsListService_address = "http://apmdemo:8081/introscope-web-services/services/MetricsListService";

    public java.lang.String getMetricsListServiceAddress() {
        return MetricsListService_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MetricsListServiceWSDDServiceName = "MetricsListService";

    public java.lang.String getMetricsListServiceWSDDServiceName() {
        return MetricsListServiceWSDDServiceName;
    }

    public void setMetricsListServiceWSDDServiceName(java.lang.String name) {
        MetricsListServiceWSDDServiceName = name;
    }

    public br.com.caapm.wsdl.IMetricsListService getMetricsListService() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MetricsListService_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMetricsListService(endpoint);
    }

    public br.com.caapm.wsdl.IMetricsListService getMetricsListService(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            br.com.caapm.wsdl.MetricsListServiceSoapBindingStub _stub = new br.com.caapm.wsdl.MetricsListServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getMetricsListServiceWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMetricsListServiceEndpointAddress(java.lang.String address) {
        MetricsListService_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (br.com.caapm.wsdl.IMetricsListService.class.isAssignableFrom(serviceEndpointInterface)) {
                br.com.caapm.wsdl.MetricsListServiceSoapBindingStub _stub = new br.com.caapm.wsdl.MetricsListServiceSoapBindingStub(new java.net.URL(MetricsListService_address), this);
                _stub.setPortName(getMetricsListServiceWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("MetricsListService".equals(inputPortName)) {
            return getMetricsListService();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:com.wily.introscope.server.webservicesapi.metricslist", "MetricsListService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:com.wily.introscope.server.webservicesapi.metricslist", "MetricsListService"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("MetricsListService".equals(portName)) {
            setMetricsListServiceEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
