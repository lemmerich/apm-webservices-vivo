/**
 * IMetricsListService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package br.com.caapm.wsdl;

public interface IMetricsListService extends java.rmi.Remote {
    public br.com.caapm.wsdl.Metric[] listMetricsForMetricPath(java.lang.String agentRegex, java.lang.String metricPath) throws java.rmi.RemoteException, br.com.caapm.wsdl.IntroscopeWebServicesException;
    public br.com.caapm.wsdl.MetricPath[] listMetricPaths(java.lang.String agentRegex, java.lang.String metricPrefix, boolean recursive) throws java.rmi.RemoteException, br.com.caapm.wsdl.IntroscopeWebServicesException;
    public java.lang.String[] getDomainsForAgent(java.lang.String agentRegex) throws java.rmi.RemoteException, br.com.caapm.wsdl.IntroscopeWebServicesException;
    public br.com.caapm.wsdl.Metric[] listMetrics(java.lang.String agentRegex, java.lang.String metricRegex) throws java.rmi.RemoteException, br.com.caapm.wsdl.IntroscopeWebServicesException;
    public java.lang.String[] listAgents(java.lang.String agentRegex) throws java.rmi.RemoteException, br.com.caapm.wsdl.IntroscopeWebServicesException;
}
