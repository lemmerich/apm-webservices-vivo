package br.com.caapm.program;

import java.io.FileWriter;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.axis.AxisFault;
import org.apache.axis.client.Service;

import br.com.caapm.dto.AgentHealthCheckDTO;
import br.com.caapm.utils.CSVUtils;
import br.com.caapm.wsdl.AgentMetricName;
import br.com.caapm.wsdl.IntroscopeWebServicesException;
import br.com.caapm.wsdl.MetricData;
import br.com.caapm.wsdl.MetricPath;
import br.com.caapm.wsdl.MetricsDataServiceSoapBindingStub;
import br.com.caapm.wsdl.MetricsListServiceSoapBindingStub;
import br.com.caapm.wsdl.TimesliceGroupedMetricData;
import br.com.caapm.wsdl.TimeslicedMetricDataPerAgentMetric;
import br.com.caapm.wsdl.TimeslicedResultPerMetricData;
import br.com.caapm.wsdl.TimeslicedResultSetMetricData;

public class Main {

	// VIVO
//	static final String SERVICES_BASE_URL = "http://10.240.2.48:8081/introscope-web-services/services/";
//	static final String METRICS_LIST_SERVICE_URL = "MetricsListService";
//	static final String METRICS_DATA_SERVICE_URL = "MetricsDataService";
//	static final String USERNAME = "admin";
//	static final String PASSWORD = "CAAPM@Vivo15";
	
	// LOCAL
	static final String SERVICES_BASE_URL = "http://apmdemo:8081/introscope-web-services/services/";
	static final String METRICS_LIST_SERVICE_URL = "MetricsListService";
	static final String METRICS_DATA_SERVICE_URL = "MetricsDataService";
	static final String USERNAME = "admin";
	static final String PASSWORD = "";
	
	static final String CSV_FILE = "C:/Users/frelu03/Desktop/report.csv";
	static final Date START_DATE = new Date(((new Date()).getTime()-15000));
	static final Date END_DATE = new Date();
	
	public static void main(String[] args) throws ParseException, IOException {

		MetricsListServiceSoapBindingStub metricsListService = createListService();
		MetricsDataServiceSoapBindingStub metricsDataService = createDataService();

		String[] listAgents;
		
		try {			
			/////////////////////////////////
			HashMap<String, AgentHealthCheckDTO> agentDTOHashMap = new HashMap<String, AgentHealthCheckDTO>();
			
			ArrayList<AgentMetricName> agentMetricNameList = new ArrayList<AgentMetricName>();
			ArrayList<String> metricsList = new ArrayList<String>();
			metricsList.add("Java Version");
			metricsList.add("Agent Stats:Build and Release");
			metricsList.add("Build and Release");
			metricsList.add("Host:IP Address");
			metricsList.add("GC Monitor:Jvm Type");
			metricsList.add("CPU:Utilization % (process)");
			
			// CHECK IF METRIC FOLDER EXISTS
			ArrayList<String> hasMetricNodeList = new ArrayList<String>();
			hasMetricNodeList.add("OSB");
			
			// CHECK Virtual Agent - Agent Metrics
			ArrayList<String> virtualAgentMetricsList = new ArrayList<String>();
			virtualAgentMetricsList.add("Metric Count");
			
			// ALL Metrics
			ArrayList<String> allMetricsList = new ArrayList<String>();
			allMetricsList.add("AgentName");
			allMetricsList.addAll(metricsList);
			allMetricsList.addAll(virtualAgentMetricsList);
			allMetricsList.addAll(hasMetricNodeList);
			
			listAgents = metricsListService.listAgents("^((?!Virtual).)*$");
			// Recupera todos os agentes não virtuais
			for (String agentNamePath : listAgents) {
				
				agentDTOHashMap.put(agentNamePath, new AgentHealthCheckDTO(agentNamePath));
				
				// Prepara array com todas as métricas que se quer recuperar
				for(String metricName: metricsList){
					AgentMetricName agentMetricName = new AgentMetricName();
					agentMetricName.setAgentName(agentNamePath);
					agentMetricName.setMetricName(metricName);
					
					agentMetricNameList.add(agentMetricName);
				}
				
				// -> METRIC COUNT
				for(String metricName: virtualAgentMetricsList){
					String agentMetricCount = "";
					TimesliceGroupedMetricData[] timesliceGroupedMetricDatas = metricsDataService.getMetricData(".*Virtual.*","Agents\\|" + agentNamePath.replace("|", "\\|") + ":"+metricName, START_DATE, END_DATE, 1);
					
					for (TimesliceGroupedMetricData timesliceGroupedMetricData : timesliceGroupedMetricDatas) {
						MetricData metricData = timesliceGroupedMetricData.getMetricData()[0];
						agentMetricCount = metricData.getMetricValue();
						break;
					}
					
					agentDTOHashMap.get(agentNamePath).getMetricsMap().put(metricName, agentMetricCount);
				}
				
				// -> CHECK IF HAS A METRIC NODE
				for (String nodeToCheck : hasMetricNodeList) {
					MetricPath[] listMetricPaths = metricsListService.listMetricPaths(Pattern.quote(agentNamePath), nodeToCheck, false);
					
					boolean hasNode = false;
					if (listMetricPaths.length > 0) {
						hasNode = true;
					}
					
					agentDTOHashMap.get(agentNamePath).getMetricsMap().put(nodeToCheck, Boolean.toString(hasNode));
				}
				
			}
			
			AgentMetricName[] agentsArray = agentMetricNameList.toArray(new AgentMetricName[agentMetricNameList.size()]);
			
			TimeslicedMetricDataPerAgentMetric[] bulkMetricData = metricsDataService.getBulkMetricData(agentsArray, START_DATE, END_DATE, 1);
			for (TimeslicedMetricDataPerAgentMetric timeslicedMetricDataPerAgentMetric : bulkMetricData) {
				String agentName = timeslicedMetricDataPerAgentMetric.getAgentMetric().getAgentName();
				String metricName = timeslicedMetricDataPerAgentMetric.getAgentMetric().getMetricName();
				
				AgentHealthCheckDTO agentHealthCheckDTO = agentDTOHashMap.get(agentName);
				String metricValue = "";
				
				TimeslicedResultPerMetricData[] timeslicedMetricData = timeslicedMetricDataPerAgentMetric.getTimeslicedMetricData();
				
				if(timeslicedMetricData == null || timeslicedMetricData.length<1){
					agentHealthCheckDTO.getMetricsMap().put(metricName, "");
				}else{
					for (TimeslicedResultPerMetricData timeslicedResultSetMetricData : timeslicedMetricData) {
						metricValue = timeslicedResultSetMetricData.getMetricValue();
						
						agentHealthCheckDTO.getMetricsMap().put(metricName, metricValue);
						
						// Não queremos pegar todos os time slices, apenas um é suficiente para pegar o live...
						break;
					}
				}
			}
			
			Set<String> agentKeySet = agentDTOHashMap.keySet();
			for (String agentKey : agentKeySet) {
				AgentHealthCheckDTO agentHealthCheckDTO = agentDTOHashMap.get(agentKey);
				HashMap<String, String> metricsMap = agentHealthCheckDTO.getMetricsMap();
				Set<String> metricKeySet = metricsMap.keySet();
				
				System.out.println("Agent Name: [ "+agentKey+" ]");
				for (String metricKey : metricKeySet) {
					String metricValue = metricsMap.get(metricKey);
					
					System.out.println("Metric Name: "+metricKey+" | "+" Metric Value: "+metricValue);
				}
				
				System.out.println("");
			}
			
			///////////////////////////////////
			
			String csvFile = CSV_FILE;
			FileWriter writer = new FileWriter(csvFile);

			CSVUtils.writeLine(writer, allMetricsList);
			allMetricsList.remove(0);
			for (String agentNamePath : listAgents) {
				
				AgentHealthCheckDTO agentHealthCheckDTO = agentDTOHashMap.get(agentNamePath);
				
				ArrayList<String> metricValues = new ArrayList<String>();
				metricValues.add(agentNamePath);
				
				for (String metricNameKey : allMetricsList) {
					String value = agentHealthCheckDTO.getMetricsMap().get(metricNameKey);
					
					if(value==null){
						value="";
					}
					metricValues.add(value);
				}
				
				CSVUtils.writeLine(writer,metricValues);
			}
			writer.flush();
			writer.close();

			System.out.println("FIM DE EXECUCAO");

		}catch(IntroscopeWebServicesException e){
			// 	TODO Auto-generated catch block
			e.printStackTrace();
		}catch(RemoteException e){
			// 	TODO Auto-generated catch block
			e.printStackTrace();
		}catch(IOException e){
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	static MetricsListServiceSoapBindingStub createListService() {
		MetricsListServiceSoapBindingStub webserviceStub = null;
		try {
			webserviceStub = new MetricsListServiceSoapBindingStub(
					new URL(SERVICES_BASE_URL + METRICS_LIST_SERVICE_URL), new Service());

			webserviceStub.setUsername(USERNAME);
			webserviceStub.setPassword(PASSWORD);
		} catch (AxisFault e) {
			System.out.println(e.getFaultString());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return webserviceStub;
	}

	static MetricsDataServiceSoapBindingStub createDataService() {
		MetricsDataServiceSoapBindingStub webserviceStub = null;
		try {
			webserviceStub = new MetricsDataServiceSoapBindingStub(
					new URL(SERVICES_BASE_URL + METRICS_DATA_SERVICE_URL), new Service());

			webserviceStub.setUsername(USERNAME);
			webserviceStub.setPassword(PASSWORD);
		} catch (AxisFault e) {
			System.out.println(e.getFaultString());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return webserviceStub;
	}
}
