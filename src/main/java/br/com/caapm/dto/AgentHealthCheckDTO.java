package br.com.caapm.dto;

import java.util.HashMap;

public class AgentHealthCheckDTO {
	
	private String agentNamePath = "";
	
	private HashMap<String, String> metricsMap = new HashMap<String, String>();
	
	public AgentHealthCheckDTO(String agentNamePath) {
		super();
		this.agentNamePath = agentNamePath;
	}

	public String getAgentNamePath() {
		return agentNamePath;
	}

	public HashMap<String, String> getMetricsMap() {
		return metricsMap;
	}
	
	

}
