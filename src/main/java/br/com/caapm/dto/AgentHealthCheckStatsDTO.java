package br.com.caapm.dto;

public class AgentHealthCheckStatsDTO {
	
	private String agentNamePath = "";
	
	private String metricCount = "";
	
	private String agentVersion = "";
	
	private String javaVersion = "";
	
	private String javaVendor = "";
	
	private String hostIpAddress = "";
	
	public String getJavaVendor() {
		return javaVendor;
	}

	public void setJavaVendor(String javaVendor) {
		this.javaVendor = javaVendor;
	}

	public String getHostIpAddress() {
		return hostIpAddress;
	}

	public void setHostIpAddress(String hostIpAddress) {
		this.hostIpAddress = hostIpAddress;
	}

	public String getJavaVersion() {
		return javaVersion;
	}

	public void setJavaVersion(String javaVersion) {
		this.javaVersion = javaVersion;
	}

	private boolean isOsb = false;

	public String getAgentNamePath() {
		return agentNamePath;
	}

	public void setAgentNamePath(String agentName) {
		this.agentNamePath = agentName;
	}

	public String getMetricCount() {
		return metricCount;
	}

	public void setMetricCount(String metricCount) {
		this.metricCount = metricCount;
	}

	public String getAgentVersion() {
		return agentVersion;
	}

	public void setAgentVersion(String agentVersion) {
		this.agentVersion = agentVersion;
	}

	public boolean isOsb() {
		return isOsb;
	}

	public void setOsb(boolean isOsb) {
		this.isOsb = isOsb;
	}
	
}
